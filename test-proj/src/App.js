import Maps from "./pages/Maps";
import MUI from "./pages/MUI";
import Graphs from "./pages/Graphs";
import Files from "./pages/Files";
import { Navbar } from "./components/ui/Navbar";
import { Routes, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/maps" element={<Maps />} />
        <Route path="/mui" element={<MUI />} />
        <Route path="/graphs" element={<Graphs />} />
        <Route path="/files" element={<Files />} />
      </Routes>
    </div>
  );
}

export default App;
