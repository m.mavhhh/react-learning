import React from "react";
import { Link } from "react-router-dom";
import { AppBar, Toolbar, Button } from "@mui/material";

export const Navbar = () => {
  return (
    <div>
      <AppBar>
        <Toolbar>
          <Button sx={{ color: "white", display: "block" }}>
            <Link style={{ color: "white", textDecoration: "none" }} to="/maps">
              Maps
            </Link>
          </Button>
          <Button sx={{ color: "white", display: "block" }}>
            <Link style={{ color: "white", textDecoration: "none" }} to="/mui">
              MUI
            </Link>
          </Button>
          <Button sx={{ color: "white", display: "block" }}>
            <Link
              style={{ color: "white", textDecoration: "none" }}
              to="/graphs"
            >
              Graphs
            </Link>
          </Button>
          <Button sx={{ color: "white", display: "block" }}>
            <Link
              style={{ color: "white", textDecoration: "none" }}
              to="/files"
            >
              Files
            </Link>
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};
