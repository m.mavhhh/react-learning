import * as React from "react";
import { Draggable } from "react-beautiful-dnd";
import { ListItem, ListItemText, ListItemIcon } from "@mui/material";

const DraggableListItem = ({ item, index, handleClick, activeStyle }) => {
  return (
    <Draggable draggableId={item.id} index={index}>
      {(provided, snapshot) => (
        <ListItem
          onClick={() => {
            handleClick(item.id);
          }}
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={{
            ...provided.draggableProps.style,
            borderRadius: "4px",
            border: "2px solid rgba(0,0,0,0)",
            background: snapshot.isDragging
              ? "rgba(225,225,225, 0.85)"
              : "none",
            ...activeStyle,
          }}
        >
          <ListItemIcon>
            <img
              src={item.img}
              alt=""
              style={{ height: "34px", borderRadius: "4px", marginRight: "10px" }}
            />
          </ListItemIcon>
          <ListItemText primary={item.text} />
        </ListItem>
      )}
    </Draggable>
  );
};

export default DraggableListItem;
