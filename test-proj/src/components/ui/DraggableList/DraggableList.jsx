import React from "react";
import { useState } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import DraggableListItem from "./DraggableListItem";
import { IconButton } from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import ReplayIcon from "@mui/icons-material/Replay";
import KeyboardDoubleArrowDownIcon from '@mui/icons-material/KeyboardDoubleArrowDown';

import styles from "./DraggableList.module.css";

const imgurl =
  "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1f/ad/7f/16/caption.jpg?w=1200&h=-1&s=1";
const startList = [
  { id: "0", order: 0, text: "Layer 0", img: imgurl },
  { id: "1", order: 1, text: "Layer 1", img: imgurl },
  { id: "2", order: 2, text: "Layer 2", img: imgurl },
  { id: "3", order: 3, text: "Layer 3", img: imgurl },
  { id: "4", order: 4, text: "Layer 4", img: imgurl },
];

const activeStyle = {
  color: "#000",
  border: "2px solid #aaa",
};

export default function DraggableList() {
  const [list, setList] = useState(startList);
  const [activeItemId, setActiveItemId] = useState();
  const newDefaultItem = (i) => {
    return { id: `${i}`, order: i, text: `Layer ${i}`, img: "" };
  };
  const handleClick = (itemId) => {
    console.log(itemId);
    setActiveItemId(itemId);
  };
  const handleOnDragEnd = (result) => {
    const items = Array.from(list);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    setList(items);
  };
  return (
    <div
      className={styles.wrapper}
      onClick={() => {
        setActiveItemId(null);
      }}
    >
      <DragDropContext onDragEnd={handleOnDragEnd}>
        <Droppable droppableId="droppable">
          {(provided) => (
            <ul
              onClick={(e) => {
                e.stopPropagation();
              }}
              style={{ listStyleType: "none", padding: "0px", margin: "0" }}
              ref={provided.innerRef}
              {...provided.droppableProps}
              className= {`droppable ${styles.list}`}
            >
              {list.map((item, index) => (
                <DraggableListItem
                  handleClick={handleClick}
                  item={item}
                  index={index}
                  key={item.id}
                  activeStyle={item.id == activeItemId ? activeStyle : {}}
                />
              ))}
              {provided.placeholder}
            </ul>
          )}
        </Droppable>
      </DragDropContext>
      <div className={styles.menu}>
        <IconButton
          onClick={() => {
            setList([...list, newDefaultItem(Math.max(...list.map(item => item.id))+1)]);
          }}
        >
          <AddCircleOutlineIcon />
        </IconButton>
        <IconButton
          onClick={() => {
            setList(startList);
          }}
        >
          <ReplayIcon />
        </IconButton>
        <IconButton
          disabled={!list.length}
          onClick={() => {
            setList(list.sort((a,b) => a.order - b.order));
          }}
        >
          <KeyboardDoubleArrowDownIcon />
        </IconButton>
        <IconButton
          disabled={!activeItemId}
          onClick={() => {
            setList(list.filter((item) => item.id != activeItemId));
          }}
        >
          <DeleteOutlineIcon />
        </IconButton>
      </div>
    </div>
  );
}
