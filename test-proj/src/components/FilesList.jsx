import React from "react";
import { List, ListItemText, ListItem } from "@mui/material";

export const FilesList = ({ files, ...props }) => {
  if (!files.length) {
    return <div></div>;
  }
  console.log('get files: ', files);

  return (
      <List sx={{ bgcolor: '#333', color: 'white', marginTop: '1rem', borderRadius: '8px' }}>
        {
          files.map((file, index) => (
            <ListItem key={index}>
              <ListItemText primary={file.name} />
            </ListItem>
          ))
        }
      </List>
  );
};
