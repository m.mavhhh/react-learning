import React from "react";
import { YMaps, Map, Placemark } from "@pbe/react-yandex-maps";

const defaultState = {
  center: [55.751574, 37.573856],
  zoom: 9,
};

const YaMap = (props) => {
  return (
    <div style={{ borderRadius: "16px", overflow: "hidden" }}>
      <YMaps>
        <Map defaultState={defaultState} width={500} height={500}>
          <Placemark geometry={[55.684758, 37.738521]} />
        </Map>
      </YMaps>
    </div>
  );
};

export default YaMap;
