import React from "react";
import { Map, Marker } from "pigeon-maps";

function PigeonMap() {
  return (
    <div style={{borderRadius: '16px', overflow: 'hidden'}}>
      <Map
        height={500}
        width={500}
        defaultCenter={[55.751574, 37.573856]}
        defaultZoom={9}
      >
        <Marker width={50} anchor={[50.879, 4.6997]} />
      </Map>
    </div>
  );
}

export default PigeonMap;
