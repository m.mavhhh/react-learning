import { Button } from "@mui/material";
import { useState } from "react";
import { FilesList } from "../components/FilesList";


const Files = () => {
  const [files, setFiles] = useState([]);

  return (
    <div style={{ marginTop: "5rem", marinLeft: "30px", display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
      <Button variant="contained" component="label" sx={{ width: '10rem'}}>
        <input
          id="inputFiles"
          type="file"
          multiple
          hidden
          onChange={(event) => {
            setFiles(Array.from(event.target.files));
          }}
        />
        Выбрать файлы
      </Button>
      <FilesList files={files} />
    </div>
  );
};

export default Files;
