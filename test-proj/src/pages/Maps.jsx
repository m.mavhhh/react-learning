import PigeonMap from "../components/PigeonMap";
import YaMap from "../components/YaMap";
import { Stack, Box } from "@mui/material";

function Maps() {
  return (
    <Stack
      style={{ marginTop: "4rem", marginLeft: "1rem" }}
      className="map-wrapper"
      direction="row"
      spacing={3}
    >
      <YaMap />
      <PigeonMap />
    </Stack>
  );
}

export default Maps;
