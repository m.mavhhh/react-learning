import Plot from "react-plotly.js";

const Graphs = () => {
  return (
    <div style={{ marginTop: "4rem" }}>
      <Plot
        style={{ display: "block" }}
        data={[
          {
            x: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            y: [2, 5, 3, 4, 5, 5, 6, 6, 4],
            type: "scatter",
            mode: "lines",
            marker: { color: "#1976D2" },
          },
          {
            type: "bar",
            x: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            y: [2, 5, 3, 4, 5, 5, 6, 6, 4],
          },
        ]}
        layout={{ width: "60%", height: 400, title: "A Fancy Plot" }}
      />
      <Plot
        style={{ display: "block" }}
        data={[
          {
            values: [12, 13, 25, 50],
            labels: ["Renault", "Lada", "Toyota", "Fiat"],
            type: "pie",
          },
        ]}
        layout={{ width: "60%", height: 400, title: "A Fancy Plot" }}
      />
    </div>
  );
};

export default Graphs;
