import {
  Typography,
  Stack,
  Button,
  IconButton,
  ButtonGroup,
  ToggleButtonGroup,
  ToggleButton,
  TextField,
  InputAdornment,
} from "@mui/material";
import SendIcon from "@mui/icons-material/Send";
import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";
import { useState } from "react";
import Calendar from "../components/ui/Calendar";
import RangeSlider from "../components/ui/RangeSlider";
import DataTable from "../components/DataTable";
import DraggableList from "../components/ui/DraggableList/DraggableList";

const MUI = () => {
  const [formats, setFormats] = useState([]);
  const handleFormatChange = (event, updatedFormats) => {
    setFormats(updatedFormats);
  };

  return (
    <div style={{ marginTop: "70px" }}>
      <DraggableList />
      <Stack direction="row" spacing={3} style={{ marginTop: "5rem" }}>
        <Stack direction="column" spacing={2}>
          <Typography variant="h2"> Calendar </Typography>
          <Typography variant="subtitle2"> Subtitle 2 </Typography>
          <Typography variant="body1" gutterBottom>
            {" "}
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi
            libero ea eligendi nulla reprehenderit aspernatur esse perferendis
            laborum velit adipisci quidem ratione eum earum magnam nam beatae,
            cum dolorem repudiandae?{" "}
          </Typography>
          <Typography variant="body2" gutterBottom>
            {" "}
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae
            similique id rem consequatur repellendus, accusantium dolores, eos
            provident vel harum iste quaerat quam ut quibusdam sequi obcaecati
            error assumenda ex.{" "}
          </Typography>
          <Stack spacing={2} direction="row">
            <Button variant="contained" href="/blank" color="primary">
              Primary
            </Button>
            <Button variant="contained" color="secondary">
              Secondary
            </Button>
            <Button variant="contained" color="error">
              Error
            </Button>
            <Button variant="contained" color="info">
              Info
            </Button>
            <Button variant="contained" color="warning">
              Warning
            </Button>
            <Button variant="contained" color="success">
              Success
            </Button>
            <Button variant="text" color="success">
              Text
            </Button>
            <Button variant="outlined" color="success">
              Outlined
            </Button>
          </Stack>
          <Stack direction="row" spacing={2}>
            <Button variant="contained" startIcon={<SendIcon />} disableRipple>
              Send
            </Button>
            <Button variant="contained" endIcon={<SendIcon />} disableElevation>
              Send
            </Button>
          </Stack>
        </Stack>
        <RangeSlider height={300} />
        <Calendar />
      </Stack>

      <Stack spacing={4} direction="column">
        <Stack display="block" spacing={2} direction="row">
          <Button variant="contained" size="small">
            Small
          </Button>
          <Button variant="contained" size="medium">
            Medium
          </Button>
          <Button variant="contained" size="large">
            Large
          </Button>
          <IconButton
            aria-label="send"
            color="success"
            size="large"
            onClick={() => alert("clicked")}
          >
            <SendIcon />
          </IconButton>
        </Stack>
        <Stack direction="row">
          <ButtonGroup
            variant="outlined"
            orientation="vertical"
            size="small"
            color="secondary"
          >
            <Button>Left</Button>
            <Button>Center</Button>
            <Button>Right</Button>
          </ButtonGroup>
        </Stack>
        <Stack direction="column" spacing={2}>
          <ToggleButtonGroup
            size="small"
            value={formats}
            onChange={handleFormatChange}
          >
            <ToggleButton value="bold">
              <FormatBoldIcon />
            </ToggleButton>
            <ToggleButton value="italic">
              <FormatItalicIcon />
            </ToggleButton>
            <ToggleButton value="underlined">
              <FormatUnderlinedIcon />
            </ToggleButton>
          </ToggleButtonGroup>
          <Stack direction="row" spacing={2}>
            <TextField
              label="Amount"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">$</InputAdornment>
                ),
              }}
            />
            <TextField label="Name" variant="filled" />
            <TextField label="Name" variant="standard" />
            <TextField label="Error" size="small" error />
          </Stack>
          <Stack direction="row" spacing={2}>
            <TextField label="Name" required />
            <TextField label="Name" helperText="Your parent name" />
            <TextField label="Password" type="password" />
            <TextField label="Disabled" disabled />
            <TextField label="Read only" inputProps={{ readOnly: true }} />
          </Stack>
        </Stack>
        <DataTable />
      </Stack>
    </div>
  );
};

export default MUI;
